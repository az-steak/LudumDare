define(['../states', '../base'], function (State, Base) {

    function run () {}

    function Module (params) {
        var instance = this
        this.name = params.name || 'name'
        this.id = params.id || undefined
        this.state = params.state || State.OK
        this.consumption = params.consumption || 0
        this.setModulesPower(typeof(params.initialFactor) === 'undefined' ? 1 : params.initialFactor)
        this.production = params.production || 0
        this.icon = params.icon || null
        this.brokenIcon = params.brokenIcon || null
        this.sliderLabel = params.sliderLabel || ""
        this.repairTime = 0
        this.spinner
        this.repairButton

        this.currentSliderFactor = this.consumptionFactor

        this.run = (params.run) ? function (base) {
            if (this.repairTime > 0) {
                this.repairTime--
                if (this.repairTime <= 0) {
                    this.repair(base)
                }
                return 0
            }

            if (this.state !== State.BROKEN){
                if (base.energy < this.consumption * Math.abs(this.currentSliderFactor) ){
                    this.turnOffModule();
                }
                else {
                    this.turnOnModule();
                }
            }

            params.run(base, instance)
            
            if(this.state === State.OFF || this.state === State.BROKEN){
                return 0;
            }


            base.removeEnergy(this.getConsumption())
            var totalProduction = this.getProduction() - this.getConsumption();
            $('#consumption'+this.id).text(totalProduction +' MW');

            return totalProduction;

        } : run

        this.display = params.display(this)

        this.breakOptional = params.break
        this.repairOptional = params.repair
    }

    Module.prototype.break = function (base) {
        if (this.breakOptional) {
            this.breakOptional(base)
        }

        this.state = State.BROKEN
        this.currentSliderFactor = this.consumptionFactor;
        this.setModulesPower(0);


        $("#"+this.id).addClass("broken");
        $("#"+this.id+" > input").addClass("broken");

        $("#"+this.id+" > div > img").attr("src",this.brokenIcon);
        $("#"+this.id+" > input").prop('disabled', true);

        base.sendLog('Warning, signal lost for module ' + this.id + ' ─ ' + this.name);

        return this.openRepairButton(base)
    }

    Module.prototype.repair = function (base) {
        this.spinner.remove()

        var room = base.findRoomOfModule(this)
        if(room){
            room.endRepair(this.icon);
        }

        if (this.repairOptional) {
            this.repairOptional(base)
        }

        this.state = State.OK
        this.setModulesPower(this.currentSliderFactor);
        
        $("#"+this.id).removeClass("broken");
        $("#"+this.id+" > input").removeClass("broken");

        $("#"+this.id+" > div > img").attr("src", this.icon);
        $("#"+this.id+" > input").prop('disabled', false);

        base.sendLog('Signal recovered for module '+this.id+' ─ '+this.name);
    }

    Module.prototype.openRepairButton = function (base) {
        var instance = this
        var repairCost = Math.round((400 + Math.random() * 200) * 10) / 10
        instance.repairButton = $('<span class="repairButton">Repair ─ cost : ' + repairCost + ' MW</span>')
        instance.repairButton.click(function () {
            if (base.energy >= repairCost) {
                var room = base.findRoomOfModule(instance)
                if(room){
                    room.lunchRepairGUI();
                }
                instance.lunchRepair(instance, base, repairCost)
            }
        })

        this.display.append(instance.repairButton)

        return repairCost;
    }

    Module.prototype.lunchRepair = function(instance, base, repairCost){

        if (base.energy >= repairCost) {
            instance.repairButton.remove();
            instance.spinner = $('<img src="./res/icon/loadingBroken.gif" class="repairSpinner"></img>')
            var repairTime = 30 + Math.random() * 30

            instance.display.append(instance.spinner)
            base.removeEnergy(repairCost)
            instance.repairTime = repairTime
        }
    }


    Module.prototype.setModulesPower = function (value) {
        if(this.state === State.OFF || this.state === State.BROKEN){
            this.consumptionFactor = 0
            return;
        }

        this.consumptionFactor = value
    }

    Module.prototype.getConsumption = function () {
        return (this.state === State.BROKEN) ? 0 : this.consumption * Math.abs(this.consumptionFactor)
    }

    Module.prototype.getProduction = function () {
        return (this.state === State.BROKEN) ? 0 : this.production * this.consumptionFactor
    }

    Module.prototype.turnOffModule = function () {
        if(this.state === State.OFF || this.state === State.BROKEN){
            return;
        }

        this.state = State.OFF

        this.currentSliderFactor = this.consumptionFactor;
        this.setModulesPower(0);

        $("#"+this.id).addClass("broken");
        $('#consumption'+this.id).text('Not enough power!');
    }

    Module.prototype.turnOnModule = function () {
        if(this.state === State.OK || this.state === State.BROKEN){
            return;
        }

        this.state = State.OK

        this.setModulesPower(this.currentSliderFactor);

        $("#"+this.id).removeClass("broken");
    }

    return Module
})
