<?php
    function loadFilesIn ($dir) {
        $paths = scandir($dir); // On récupère tous les éléments du dosser $dir
        array_splice($paths, 0, 2); // on vire . et ..

        $files = array();

        foreach ($paths as $path) { // on parcours les éléments
            $path = $dir . "/" . $path; // on concatene pour avoir la chemin complet

            if (is_dir($path)) {
                $files = array_merge($files, loadFilesIn($path)); // si l'élément est un dossier, on rappelle la fonction sur lui
            } else {
                array_push($files, $path);  //sinon on ajoute simplement le fichier
            }
        }

        return $files;
    }

    $dirs = array(
        "../res"
    );

    $res = array();
    foreach ($dirs as $key) {
        $res = array_merge($res,loadFilesIn($key));
    }
    echo json_encode($res);
?>
