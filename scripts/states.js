define([], function () {

    return {
        OK: 'ok',
        OFF: 'off',
        BROKEN: 'broken'
    }
})
