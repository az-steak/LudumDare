define([], function () {

    return {
        size: 5,
        level: [
            null, 'room', null, null, null,
            null, 'room', null, 'Shield', null,
            'Oxygen generator', 'Core', 'room', 'room', 'room',
            null, null, 'Time warp', null, null,
            null, null, 'room', null, null,
        ]
    }
})
