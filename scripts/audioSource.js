define([], function () {

    var backgroundSound = {}

    var loadedSounds = {}

    function AudioSource () {

    }

    function getSound (path) {
        if (!loadedSounds[path]) {
            loadedSounds[path] = new Audio(path)
        }

        return loadedSounds[path]
    }

    AudioSource.playBackground = function (filePath, id) {
        id = id || 0

        if (backgroundSound[id]) {
            backgroundSound[id].currentTime = 0
            backgroundSound[id].pause()
        }

        backgroundSound[id] = new Audio(filePath)
        backgroundSound[id].play()




        $(backgroundSound[id]).on("loadedmetadata", function () {
            setTimeout(function () {
                AudioSource.playBackground(filePath, id)
            }, backgroundSound[id].duration * 1000)
        })
    }

    AudioSource.stopBackground = function (id) {
        id = id || 0
        if (backgroundSound[id]) {
            backgroundSound[id].pause()
        }
    }

    AudioSource.playOneShot = function (filePath, onComplete) {
        var audio = new Audio(filePath)
        audio.play()

        if (onComplete) {
            audio.onended = onComplete
        }
    }

    return AudioSource
})
