define(['jquery', 'states', 'base'], function ($, State, Base) {
    var cursorsStep = 0.05

    function displaySlider(instance, initialValue, min, max)
    {
        min = min || 0
        max = (typeof max === 'undefined') ? 1 : max
        var display = $('<div id="'+instance.id+'" class="div_control"></div>')
        var controller = $('<input id="' + instance.name + '" type="range" min="' + min + '" max="' + max + '" step="' + cursorsStep + '" value="'+initialValue+'"></input>')
        var icon = $('<img class="controlIcon" src="'+instance.icon+'"/>')
        controller.on('input change', function (event) {
            instance.currentSliderFactor = event.target.valueAsNumber;
            instance.setModulesPower(event.target.valueAsNumber)
        })

        var container = $('<div class="controlContainer"></div>')
        container.append(icon);
        container.append( $('<span class="controlName">' + instance.name + '</span>'))
        container.append(" ─ ")
        container.append( $('<span id="consumption'+instance.id+'" class="consumption"> loading data... </span>'))

        display.append(container)
        display.append('<span class="sliderLabel">'+instance.sliderLabel+'</span>')
        display.append(controller)

        return display;
    }

    return [
        {
            name: 'Core',
            id: '0x0000ff',
            consumption: 20,
            icon: 'res/icon/Core0.png',
            brokenIcon: 'res/icon/Deffectueux/Core0 Deffectueux.png',
            sliderLabel: 'Overclock',
            run: function (base, instance) {
                if (instance.consumptionFactor < 1) {
                    $('#eventList').addClass('off').removeClass('text-left')
                } else {
                    $('#eventList').removeClass('off').addClass('text-left')
                }

                if (instance.consumptionFactor < 0.5) {
                    $('#logList').addClass('off').removeClass('text-left')
                } else {
                    $('#logList ').removeClass('off').addClass('text-left')
                }

                if (instance.consumptionFactor < 0.2 ) {
                    $('#statusList').addClass('off').removeClass('text-left')
                } else {
                    $('#statusList').removeClass('off').addClass('text-left')
                }

                if (instance.consumptionFactor === 0) {
                    $('#hider').addClass('show')
                } else {
                    $('#hider').removeClass('show')
                }
            },
            break: function (base) {
                $('#eventList').addClass('off').removeClass('text-left')
                $('#logList').addClass('off').removeClass('text-left')
                $('#statusList').addClass('off').removeClass('text-left')
            },
            repair: function (base) {
                $('#eventList').removeClass('off').addClass('text-left')
                $('#logList').removeClass('off').addClass('text-left')
                $('#statusList').removeClass('off').addClass('text-left')
            },
            display: function (instance) {
                return displaySlider(instance, 1)
            }
        },
        {
            name: 'Oxygen generator',
            id: '0xdfa84f',
            consumption: 8,
            production: 0,
            icon: 'res/icon/O2.png',
            brokenIcon: 'res/icon/Deffectueux/O2 Deffectueux.png',
            sliderLabel: 'Power allocation',
            run: function (base, instance) {
                var noise = 0.1
                var oxygenProduction = 320
                var value = oxygenProduction * instance.consumptionFactor

                value += value * (Math.random() - 0.5) * 2 * noise
                base.addOxygen(value)
            },
            display: function (instance) {
                return displaySlider(instance, 1)
            },
        },
        {
            name: 'Shield',
            id: '0x58a7cf',
            consumption: 10,
            initialFactor: 0,
            icon: 'res/icon/Shield.png',
            brokenIcon: 'res/icon/Deffectueux/Shield Deffectueux.png',
            sliderLabel: 'Coil loading',
            run: function () {
            },
            display: function (instance) {
                return displaySlider(instance, 0)
            }
        },
        {
            name: 'Time warp',
            id: '0x0221bf',
            consumption: 10,
            initialFactor: 0,
            icon: 'res/icon/TimeWarp.png',
            brokenIcon: 'res/icon/Deffectueux/TimeWarp.png',
            sliderLabel: 'Flux capacitor',
            run: function (base, instance) {
                base.setTimeSpeed(instance.consumptionFactor)
            },
            display: function (instance) {
                return displaySlider(instance, 0, -1, 1)
            }
        },
        {
            name: 'Solar pannels',
            id: '0xf3de5f',
            consumption: 0,
            production: 40,
            icon: 'res/icon/SollarPannel.png',
            brokenIcon: 'res/icon/Deffectueux/SollarPannel Deffectueux.png',
            sliderLabel: 'Deployment',
            run: function (base, instance) {
                if(base.dateGame.getHours() < 14){
                    var production = instance.consumptionFactor * instance.production
                    base.addEnergy(production)
                }
                else {
                    instance.setModulesPower(0);
                }
            },
            display: function (instance) {
                return displaySlider(instance, 1)
            },
        }


    ]
})
