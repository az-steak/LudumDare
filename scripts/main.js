require.config({
    paths: {
        jquery: './ext_libs/jquery'
    }
})

require(['./base', './events/event', 'audioSource'], function (Base, Event, AudioSource) {
    var base
    var eventProba = 0.005
    var difficulty = 0.00001
    var gameLaunched = false

    $.ajax({
        url: './scripts/loadRes.php',
        dataType: 'json',
        success: function (paths) {
            loadFiles(paths)
        },
        error: function (error) {
            console.log(error)
        }
    })

    function bindButtons () {
        $('#playButton').click(function () {
            $('#mainMenu').remove()
            $('#main').removeClass('hide')
            launchGame()
        })

        $('#instructionButton').click(function () {
            $('#instructions').removeClass('hide')
        })

        $('#creditButton').click(function () {
            $('#credits').removeClass('hide')
        })


        $('#instructions').click(function () {
            $('#instructions').addClass('hide')
        })

        $('#credits').click(function () {
            $('#credits').addClass('hide')
        })
    }

    function loadComplete() {
        $('#loading').remove()
        $('#nav').removeClass('hide ')
        initSounds()
    }

    function initSounds() {
        AudioSource.playOneShot('./res/sounds/computerStarting.mp3')
        $(window).click(function () {
            AudioSource.playOneShot('./res/sounds/keyboardType.mp3')
        })
    }

    function loadFiles (paths) {
        var filesCount = paths.length
        var filesLoaded = 0

        for (var i = 0; i < paths.length; i++) {
            var path = paths[i].substring(1)

            $.ajax({
                url: path,
                success: function (data) {
                    filesLoaded++
                    $('#progressLoad #content').css({
                        width: filesLoaded / filesCount * 100 + '%'
                    })

                    if (filesLoaded === filesCount) {
                        loadComplete()
                    }
                },
                error: function (data) {
                    console.log(data)
                }
            })
        }
    }

    function launchGame () {
        base = new Base()

        gameLoop()
        initSoundsGame()
        checkEvents()
    }

    function initSoundsGame () {
        AudioSource.playBackground('./res/sounds/ambiant.mp3')
        AudioSource.playBackground('./res/sounds/computerNoises.mp3', 1)
    }

    function gameLoop () {
        base.run()
    }

    function checkEvents () {
        eventProba += difficulty
        if (base.loadEnded && Math.random() < eventProba) {
            difficulty += 0.00001
            base.addEvent(Event.pickRandomEvent())
            eventProba = 0
        }
        setTimeout(checkEvents, base.timeSpeed)
    }

    bindButtons()
})
