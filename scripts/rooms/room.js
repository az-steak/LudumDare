define(['../states'], function (State) {

    function Room (params) {
        this.state = State.OK
        this.module = params.module
        this.position = params.position
        this.repairTime = 0;
        this.spinner
        this.repairButton
    }

    Room.prototype.run = function (base) {
        if (this.repairTime > 0) {
            console.log(this.repairTime)
            this.repairTime--
            if (this.repairTime <= 0) {
                this.endRepair(undefined)
            }
            return
        }
        if (this.state === State.BROKEN) {
            return
        }
    }

    Room.prototype.break = function (base) {
        var instance = this;
        this.state = State.BROKEN

        var posX = this.position.x;
        var posY = this.position.y;

        var repairCost = Math.round((100 + Math.random() * 200) * 10) / 10

        if (this.module && this.module.state === State.OK) {
             repairCost = this.module.break(base)
        }

        $("#"+posX+''+posY).html('<img src="res/icon/Disabled.png" class="disabledIcon"></img>');
        this.repairButton = $('<span class="repairButtonMap">Repair ─ cost : ' + repairCost + ' MW</span>')
        $("#"+posX+''+posY).append(this.repairButton)
        this.repairButton.click(function () {

            if (base.energy >= repairCost) {
                if(instance.module){    
                    instance.module.lunchRepair(instance.module, base, repairCost);
                }
                else {
                    var repairTime = 10 + Math.random() * 30
                    base.removeEnergy(repairCost)
                    instance.repairTime = repairTime
                }

                instance.lunchRepairGUI();
            }

        })
    }

    Room.prototype.lunchRepairGUI = function() {
        var posX = this.position.x;
        var posY = this.position.y;

        this.repairButton.remove()
        this.spinner = $('<img src="./res/icon/loadingBroken.gif" class="repairSpinnerMap"></img>')
        $("#"+posX+''+posY).html(this.spinner)
    }

    Room.prototype.endRepair = function(icon){
        this.spinner.remove();
        this.state = State.OK

        if(icon !== undefined){
            var posX = this.position.x;
            var posY = this.position.y;

            $("#"+posX+''+posY).html('<img class="mapIcon" src="'+icon+'"/>')
        }
    }

    return Room
})
