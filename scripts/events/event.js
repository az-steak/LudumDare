define(['jquery', './eventsList', '../base'], function($, eventsList, Base){


    var eventId = 0

    function doEffect (){
        this.firstRun = false
    }


    Event.pickRandomEvent = function () {
        var i = Math.floor(Math.random() * eventsList.length)
        var params = eventsList[i]

        return new Event(params)
    }


    function Event(params){
        var instance = this

        this.id = eventId
        this.firstRun = true
        this.name = params.name
        this.strenght = params.strenght
        this.announcement = params.announcement
        this.endAnnouncement = params.endAnnouncement
        this.timeStart = Base.minuteTime + (params.delay || 0)
        this.timeEnd   = this.timeStart + (params.duration || 0)
        this.setDescription(params)
        this.updateDescription()

        this.doEffect = function (base) {
            params.doEffect(base, instance)
            this.firstRun = false
        } || doEffect


        this.display = $('<div class="div_event" id="' + eventId + '"></div>')
        this.display.append('<h4>' + this.name + '</h4>')
        this.display.append(this.description)

        eventId++
    }

    Event.prototype.setDescription = function (params) {
        this.description = '<u>Danger level</u>: '

        if (params.duration > 40) {
            this.description += 'high'
        } else if (params.duration > 25) {
            this.description += 'medium'
        } else {
            this.description += 'low'
        }

        this.description += '<br>' + params.description
        this.description += '<div class="remainingTime">Estimated remaining time: <span class="value">' + Math.round(this.timeEnd - Base.minuteTime) + '</span></div>'

        this.description += '<hr>'
    }

    Event.prototype.updateDescription = function (params) {
        var timeRemaining = Math.round(this.timeStart - Base.minuteTime)
        if (timeRemaining < 0) {
            $('#'+ this.id + '.remainingTime').remove()
        } else {
            $('#' + this.id + ' .remainingTime .value').html(timeRemaining)
        }
    }


     return Event

})
