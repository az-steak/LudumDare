define([], function () {

    return [
        'Short circuits provoke energy\'s loss. The loss is proportional to our energy flow. \
        The more we produce/consume, the more we will suffer.',
        'Extended solar pannels are more likely to suffer damages from falling objects.',
        'We still don\'t know if there\'s life in outer space.',
        'If you witness a failure in one of the station\'s module, you must inform the administrator',
        'The download/upload speed is only of 200 PiB/ms, citizens must be careful when using mass relay\'s internet',
        'The station can provide up to 24h of oxygen for 100 citizens without oxygen generation'
    ]
})
