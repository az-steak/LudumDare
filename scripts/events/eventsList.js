define(['states'], function (State) {

    return [
        {
            name: 'Debris field inbound',
            description: 'A debris field is coming. prepare for impact',
            announcement: 'Warning, multiple debris incomming.',
            endAnnouncement: 'Station out of debris field. Check modules recommended...',
            delay: 100 + Math.random() * 70,
            duration: 10 + Math.random() * 50,
            doEffect: function (base, instance) {
                if (instance.firstRun) {
                    base.sendLog('We\'re hitting debris at high velocity!')
                }

                if (Math.random() < 0.2) {
                    base.breakTest()
                }
            }
        },
        {
            name: 'Solar Wind',
            description: 'A solar storm will hit the station, you should minimize our power exchange',
            announcement: 'Warning, multiple short circuits detected',
            endAnnouncement: 'Solar wind is over, consumption coming back to normal...',
            delay: 80 + Math.random() * 20,
            duration: 1,
            doEffect: function (base, instance) {
                if (instance.firstRun) {
                    var deltaEnergy = base.getAbsoluteSumConsumption() * 100
                    base.sendLog('The base go through multiple short circuits. We are loosing energy')
                    base.removeEnergy(deltaEnergy)
                }
            }
        },
        {
            name: 'Cargo',
            description: 'A cargo is bringing new resources',
            announcement: 'Prepare the base for the incoming events',
            endAnnouncement: 'They left us new citizens and needs',
            delay: 200 + Math.random() * 5,
            duration: 1,
            doEffect: function (base, instance) {
                if (instance.firstRun) {
                    base.sendLog('Welcome the cargo')
                    base.addEnergy(350);
                    base.citizen += 10;
                }
            }

        },
        {
            name: 'Alien Invasion',
            description: 'An alien has passed our security system',
            announcement: 'We have isolated it',
            endAnnouncement: 'We lost some citizens by isolating him...',
            delay: 40 + Math.random() * 10,
            duration: 1,
            doEffect: function (base, instance) {
                if (instance.firstRun) {
                    base.sendLog('Alien Invasion!')

                    base.killCitizen(Math.floor(Math.random()* 10))

                    if (Math.random() > base.modules['Shield'].consumptionFactor) {
                        var i = Math.floor(Math.random() * base.rooms.length)
                        if(base.rooms[i].state !== State.BROKEN){
                            base.rooms[i].break(base)
                        }
                    }
                }

            },

        }
    ]
})
