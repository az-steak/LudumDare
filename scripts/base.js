define([
    'jquery',
    'rooms/roomsList',
    'rooms/room',
    'modules/module',
    'modules/modulesList',
    'states',
    'audioSource',
    'hints'
],
    function ($, rooms, Room, Module, modulesList, State, AudioSource, Hints) {

    const MAX_ENERGY = 10000
    const MAX_OXYGEN = 432000 // enough O² for 100 person during 24h breathing 3l/min
    const MAX_POPULATION = 100
    const MINIMAL_ENERGY_CONSUMPTION = 5
    const ENERGY_UNIT = 'MW'
    const CONSUMPTION_UNIT = 'MW/min'
    const OXYGEN_UNIT = 'L'
    const OXYGEN_CONSUMPTION_UNIT = 'L/min'
    const OXYGEN_CONSUMPTION_PER_HUMAN = 3
    const OXYGEN_DANGER_LEVEL = 2.5
    const DEFAULT_TIME_SPEED = 200
    const MAX_TIME_MULTIPLICATOR = 2

    const STAR_NAME = ['Arcturus', 'Proxyma', 'Orion', 'Cepheus', 'Anteres', 'Polux'];
    const GREEK_LETTER = ['Alpha', 'Beta', 'Gamma', 'Delta', 'Epsilon', 'Zeta', 'Theta', 'Sigma', 'Omega'];

    var roomHeight = 20;
    var roomWidth = 20;

    function Base () {

        $('#hider').removeClass('show')

        this.gameOver = false
        this.modules = {}
        this.rooms = []
        this.events = []
        this.energy = MAX_ENERGY
        this.oxygen = MAX_OXYGEN
        this.citizen = MAX_POPULATION
        this.timeSpeed = DEFAULT_TIME_SPEED
        this.baseTotalProduction = 0;
        this.minutesSinceLastHint = 0
        this.dateGame = new Date(0, 0, 0, 0, 0, 0, 0);

        this.findAName()

        this.updateDay()

        this.sendLog("Artificial Inteligence connected!")
        this.loadModules()

    }

    Base.minuteTime = 0


    Base.prototype.run = function () {
        if (this.loadEnded) {
            this.baseTotalProduction = 0;

            if (this.gameOver) {
                return
            }

            this.updateDay()
            this.runRooms()
            this.runModules()
            this.checkEvents()
            this.checkRoomsIntegrity()
            this.updateUIValues()
            this.updateTotalProduction()
            this.updateHumanRessources()
            this.updateOxygen()
            this.updateHints()

            Base.minuteTime++
        }

        var instance = this
        setTimeout(function () {
            instance.run()
        }, this.timeSpeed)
    }

    Base.prototype.findAName = function (){
        var star = STAR_NAME[ Math.floor(Math.random() * STAR_NAME.length) ]
        var letter = GREEK_LETTER[ Math.floor(Math.random() * GREEK_LETTER.length) ]

        $("#name").html(star+' '+letter);
    }


    var minTimeBetweenHints = 1000
    Base.prototype.updateHints = function () {
        this.minutesSinceLastHint++

        if (this.minutesSinceLastHint > minTimeBetweenHints && Math.random() < 0.2) {
            this.minutesSinceLastHint = 0
            var i = Math.floor(Math.random() * Hints.length)
            var message = 'Reminder : ' + Hints[i]
            this.sendLog(message)
        }
    }

    Base.prototype.setTimeSpeed = function (ratio) {
        if (ratio === 0) {
            this.timeSpeed = DEFAULT_TIME_SPEED
        } else if (ratio > 0) {
            this.timeSpeed = DEFAULT_TIME_SPEED / (MAX_TIME_MULTIPLICATOR / Math.abs(1 - ratio))
        } else {
            ratio /= 4
            this.timeSpeed = DEFAULT_TIME_SPEED * (MAX_TIME_MULTIPLICATOR / Math.abs(1 + ratio))
        }
    }

    Base.prototype.newCitizen = function (value) {
        value = value || 1
        this.citizen = Math.min(MAX_POPULATION, this.citizen + value)
    }

    Base.prototype.killCitizen = function (value) {
        value = value || 1
        this.citizen = Math.max(0, this.citizen - value)
    }

    Base.prototype.updateHumanRessources = function () {

        for (var i = 0; i < this.citizen; i++) {
            if (this.citizen >= 2 && Math.random() < 0.005) { // Naturals birth
                this.newCitizen()
            }
        }

        var oxyDangerLevel = OXYGEN_DANGER_LEVEL * this.citizen
        var ratioOxyLevel = this.oxygen / oxyDangerLevel
        if (Math.random() > ratioOxyLevel) { // Lack of Oxygen
            this.killCitizen()
            if (this.citizen <= 0) {
                this.doGameOver(' of a lack of oxygen')
            }
        }


        this.citizen = clamp(this.citizen, 0, MAX_POPULATION)
    }

    function clamp (value, min, max) {
        return Math.min(Math.max(value, min), max)
    }

    Base.prototype.updateUIValues = function () {
        $('#powerLeft').html(this.getPowerBar())
        $('#powerLeftValue').html((Math.round(this.energy * 10) / 10) + ' ' + ENERGY_UNIT)


        $('#peopleAlive').html(this.citizen)

        var oxygenProduction = (this.modules['Oxygen generator'].state === State.BROKEN) ? 0 : 320 * this.modules['Oxygen generator'].consumptionFactor
        oxygenProduction -= this.citizen * OXYGEN_CONSUMPTION_PER_HUMAN

        $('#oxygenLeft').html(this.getOxygenBar())
        $('#oxygenLeftValue').html(Math.round(this.oxygen * 10) / 10 + ' ' + OXYGEN_UNIT)
        $('#oxygenProduction').html(oxygenProduction + ' ' + OXYGEN_CONSUMPTION_UNIT)
    }

    Base.prototype.breakTest = function () {
        if (Math.random() > this.modules['Shield'].consumptionFactor) {
            if (Math.random() < 0.3 && Math.random() < this.modules['Solar pannels'].consumptionFactor && this.modules['Solar pannels'].state !== State.BROKEN) {
                this.modules['Solar pannels'].break(this)
            } else {
                var i = Math.floor(Math.random() * this.rooms.length)
                if(this.rooms[i].state !== State.BROKEN){
                    var kills = Math.ceil(Math.random() * this.citizen / 4)
                    this.killCitizen(kills)
                    this.rooms[i].break(this)
                    this.sendLog(Math.ceil(Math.random() * this.citizen / 4) + ' people died during a room collapsing')
                }
            }
        }
    }

    Base.prototype.increaseConsumption = function(){
       this.energy *= 0.75
    }


    Base.prototype.updateTotalProduction = function () {
        $("#totalProduction").html(this.baseTotalProduction + ' ' + CONSUMPTION_UNIT)
    }

    Base.prototype.updateOxygen = function () {
        this.removeOxygen(this.citizen * OXYGEN_CONSUMPTION_PER_HUMAN)
    }

    Base.prototype.updateDay = function () {
        var nightTime = 14

        var convertToMilis = Base.minuteTime * 60000;
        var isDay = this.dateGame.getHours() < nightTime

        this.dateGame = new Date(0, 0, 0, 0, 0, 0, convertToMilis);
        $("#daysSpent").text(this.dateGame.getDay()+'');

        var hours = this.dateGame.toTimeString().split(' ')[0].split(':')[0];
        var minute = this.dateGame.toTimeString().split(' ')[0].split(':')[1];

        $("#currentTime").text(hours+':'+minute);

        if(this.dateGame.getHours() < nightTime){
            $("#dayNight").text('day');
        }
        else {
            $("#dayNight").text('night');
        }

        var newIsDay = this.dateGame.getHours() < nightTime

        if (this.modules['Solar pannels'] && newIsDay !== isDay) {
            var power = $('#' + this.modules['Solar pannels'].id).find('input').val()
            this.modules['Solar pannels'].setModulesPower(power)
        }
    }

    Base.prototype.checkEvents = function () {
        var event

        for (var i = this.events.length - 1; i >= 0; i--) {
            event = this.events[i]
            event.updateDescription()
            if (event.timeStart < Base.minuteTime) {
                event.doEffect(this)
            }
            if (event.timeEnd < Base.minuteTime) {
                this.removeEvent(i)
            }
        }
    }

    Base.prototype.checkRoomsIntegrity = function () {
        var allDestroyed = true

        for (var i = 0; i < this.rooms.length; i++) {
            if (this.rooms[i].state !== State.BROKEN) {
                allDestroyed = false
                break;
            }
        }

        if (allDestroyed) {
            this.doGameOver('all rooms are destroyed, the station is collapsing')
        }
    }

    Base.prototype.loadRooms = function () {

        $('#mapContent').html("");

        for (var y = 0; y < rooms.size; y++) {
            for (var x = 0; x < rooms.size; x++) {
                var i = x + y * rooms.size

                var posX = roomWidth * x;
                var posY = roomHeight * y;

                if (rooms.level[i] !== null) {
                    var room = new Room({
                        position: {x: x, y: y},
                        module: this.modules[rooms.level[i]]
                    })
                    this.rooms.push(room)

                    var roomUI = $('<div id="'+x+''+y+'" class="room"  style="left:'+posX+'%; top:'+posY+'%;"></div>');

                    if(rooms.level[i] !== 'room'){
                        var icon = $('<img class="mapIcon" src="'+this.modules[rooms.level[i]].icon+'"/>')
                        roomUI.append(icon);
                    }

                    $('#mapContent').append(roomUI);
                }
            }
        }
        this.loadEnded = true
    }


    var indiceModule = 0
    Base.prototype.loadModules = function () {
        var instance = this

        setTimeout(function () {
            var module = new Module(modulesList[indiceModule])
            instance.addModule(module)

            $('#controls').append(module.display)

            instance.sendLog('Module loaded: '+module.id+' ─ '+module.name)
            indiceModule++

            if (indiceModule >= modulesList.length) {
                instance.loadRooms()
            } else {
                instance.loadModules()
            }
        }, 100 + Math.random() * 900)
    }

    Base.prototype.addModule = function (module) {
        this.modules[module.name] = module
    }

    Base.prototype.addEnergy = function (energy) {
        var availableSpace = MAX_ENERGY - this.energy
        var deltaEnergy = Math.min(availableSpace, energy)

        this.energy +=  deltaEnergy// can't add more if MAX_ENERGY reached

        if (this.energy === MAX_ENERGY && this.modules['batteries']) {
            this.modules['batteries'].addEnergy(energy - deltaEnergy)
        }
    }

    Base.prototype.removeEnergy = function (energy) {
        var prevEnergy = this.energy
        this.energy = Math.max(0, this.energy - energy)

        return prevEnergy !== this.energy
    }

    Base.prototype.findRoomOfModule = function (module) {
        for (var i = 0 ; i < this.rooms.length; i++) {
            if (this.rooms[i].module === module) {
                return this.rooms[i]
            }
        }
    }

    Base.prototype.addOxygen = function (value) {
        this.oxygen = Math.floor(Math.min(MAX_OXYGEN, this.oxygen + value) * 10) / 10
    }

    Base.prototype.removeOxygen = function (value) {
        this.oxygen = Math.max(0, this.oxygen - value)
    }

    Base.prototype.getPowerBar = function () {
        var ratio = this.energy / MAX_ENERGY
        var bar = $('<div class="progressBar"></div>')
        var barContent = $('<div class="progressBarContent"></div>')

        barContent.css({
            width: 201 * ratio + 'px'
        })
        bar.append(barContent)

        return bar
    }


    Base.prototype.getOxygenBar = function () {
        var ratio = this.oxygen / MAX_OXYGEN
        var bar = $('<div class="progressBar"></div>')
        var barContent = $('<div class="progressBarContent"></div>')

        barContent.css({
            width: 201 * ratio + 'px'
        })
        bar.append(barContent)

        return bar
    }

    Base.prototype.runModules = function () {
        this.baseTotalProduction = 0;

        this.updateBasicsSystem()

        for (var i in this.modules) {

            this.baseTotalProduction += this.modules[i].run(this)
        }

        this.baseTotalProduction = Math.round(this.baseTotalProduction * 10) / 10
    }

    Base.prototype.updateBasicsSystem = function () {
        var canRunBasics = this.removeEnergy(MINIMAL_ENERGY_CONSUMPTION)
        this.baseTotalProduction -= MINIMAL_ENERGY_CONSUMPTION

        if (!canRunBasics && Math.random() < 0.5) {
            this.killCitizen(Math.floor(Math.random() * 10))
            if (this.citizen === 0) {
                this.doGameOver(' the station don\'t have enough energy to run life support systems')
            }
        }
    }

    Base.prototype.getAbsoluteSumConsumption = function () {
        var flux = 0

        for (var i in this.modules) {
            flux += Math.abs(this.modules[i].getConsumption())
        }

        return flux
    }

    Base.prototype.runRooms = function() {
        for (var i = 0; i < this.rooms.length; i++) {
            this.rooms[i].run();
        }
    }

    Base.prototype.removeEvent = function (i) {
        var event = this.events[i]
        this.events.splice(i, 1)
        event.display.remove()

        if (event.endAnnouncement) {
            this.sendLog(event.endAnnouncement)
        }
    }

    Base.prototype.addEvent = function (event) {
        this.events.push(event)
        $('#eventList').append(event.display)
        this.sendLog(event.announcement)
    }

    Base.prototype.sendLog = function (logMsg){

        var hours = this.dateGame.toTimeString().split(' ')[0].split(':')[0];
        var minute = this.dateGame.toTimeString().split(' ')[0].split(':')[1];

        var time = hours+':'+minute

        var log = $('<div class="div_log">['+time+'] '+logMsg+'</div>')

        var scrollValue = document.getElementById("logList").scrollHeight;
        $("#logList").append(log).scrollTop(scrollValue);

        //var msg = new SpeechSynthesisUtterance(logMsg);
        //window.speechSynthesis.speak(msg);

        var soundI = Math.floor(Math.random() * 4)
        AudioSource.playOneShot('./res/sounds/hddWrite' + soundI + '.mp3')
    }

    Base.prototype.doGameOver = function (cause) {
        AudioSource.playOneShot('../res/sounds/denyBip.mp3')

        this.gameOver = true
        cause = cause || '[unable to find a cause]'
        var overScreen = $('<div id="gameOverScreen"><h1>GAME OVER</h1>\
        You survived ' + Math.round(Base.minuteTime / 60 * 10) / 10 + ' hour(s)\
        <div>Everyone died because ' + cause + '</div>\
        <br><br><br><br>Any key - back to menu</div>')

        $('#hider').addClass('show')
        $('#hider').removeClass('letClick')
        $('body').append(overScreen)
        $('body').on('keypress', function(event) {
            location.reload()
        })
    }

    return Base
})
